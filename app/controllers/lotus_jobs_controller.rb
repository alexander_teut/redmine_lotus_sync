class LotusJobsController < ApplicationController
  unloadable
  before_filter :require_admin

  require 'lotus_sync/helpers/lotus_jobs_helper'

  include LotusSync

  def reload
  	if !User.current.allowed_to?(:reload_lotus_job, nil, :global => true) then
 		render_403
  	end
  	call_hook(:controller_lotus_jobs_update)
    redirect_to "/lotus-jobs"
  end

  def index
  	if !User.current.allowed_to?(:view_lotus_job, nil, :global => true) then
 		render_403
  	end
    # stub for jobs_list
  end

  def lotus_jobs_list_json
  	if !User.current.allowed_to?(:view_lotus_job, nil, :global => true) then
 		render_403
  	end
    render json: LotusJobsHelper.lotus_jobs_list(params[:draw], params[:start], params[:length], params[:search], params[:order])
  end
end
