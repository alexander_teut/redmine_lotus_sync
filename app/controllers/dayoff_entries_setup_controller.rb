class DayoffEntriesSetupController < ApplicationController
  unloadable
  
  before_filter :require_login

  require 'lotus_sync/helpers/lotus_activities_helper'

  include LotusSync

  def index
  	@dayoff_entry = DayoffEntries.new(:user_id => User.current.id, :created_at => Time.now, :updated_at => Time.now, :from => Time.now, :to => Time.now)
    @dayoff_entry_post_url = url_for(:controller => 'dayoff_entries_setup', :action => 'update')
    @activity_types = activity_types
    @activity_type_default = @activity_types.first
  end

  def update
  	@dayoff_entry = DayoffEntries.new(
  		:user_id => User.current.id,
  		:created_at => Time.now,
  		:updated_at => Time.now,
  		:activity_type => params[:dayoff_entry_form_activity_type],
  		:from => params[:dayoff_entry_form_from],
  		:to => params[:dayoff_entry_form_to])

  	LotusActivitiesHelper.prepare_dayoff_entry_timerange @dayoff_entry
    
    already_exists_entry = LotusActivitiesHelper.actions_between(@dayoff_entry.from, @dayoff_entry.to)
    if !already_exists_entry.nil?
      case already_exists_entry[:type]
        when 'dayoff'
           flash[:error] = l(:label_dayoff_error_dayoff) %
                                      {
                                        from: @dayoff_entry.from.strftime("%Y-%m-%d"),
                                        to: @dayoff_entry.to.strftime("%Y-%m-%d"),
                                        activity_name: @dayoff_entry.activity_type,
                                        existing_activity_name: already_exists_entry[:activity_name],
                                        existing_activity_from: already_exists_entry[:from],
                                        existing_activity_to: already_exists_entry[:to]
                                      }
        when 'timeentry'
           flash[:error] = l(:label_dayoff_error_timeentry) %
                                      {

                                        from: @dayoff_entry.from.strftime("%Y-%m-%d"),
                                        to: @dayoff_entry.to.strftime("%Y-%m-%d"),
                                        activity_name: @dayoff_entry.activity_type,
                                        issue_date: already_exists_entry[:from],
                                        issue_activity_name: already_exists_entry[:activity_name],
                                        issue_name: already_exists_entry[:issue_name]
                                      }
      end
    elsif @dayoff_entry.save
      flash[:notice] = l(:label_dayoff_is_saved) %
                              {
                                from: @dayoff_entry.from.strftime("%Y-%m-%d"),
                                to: @dayoff_entry.to.strftime("%Y-%m-%d"),
                                activity_name: @dayoff_entry.activity_type
                              }
      call_hook(:controller_dayoff_entries_after_save,
                :dayoff_entry => {
                    :activity_type => @dayoff_entry.activity_type,
                    :from => @dayoff_entry.from,
                    :to => @dayoff_entry.to,
                    :user_id => @dayoff_entry.user_id
                  })
    end
    redirect_to "/"
  end

  def activity_types
      [ l(:field_dayoff_activity_type_illness),
        l(:field_dayoff_activity_type_training),
        l(:field_dayoff_activity_type_leave),
        l(:field_dayoff_activity_type_dayoff),
        l(:field_dayoff_activity_type_idle),
      ]
    end
end