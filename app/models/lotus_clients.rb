class LotusClients < ActiveRecord::Base
  unloadable

  attr_accessible :name, :name_short, :description, :created_at, :updated_at

end