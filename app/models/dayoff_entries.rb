class DayoffEntries < ActiveRecord::Base
  unloadable
  belongs_to :user

  attr_accessible :created_at, :updated_at, :activity_type, :from, :to, :user_id

end