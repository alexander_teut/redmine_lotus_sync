class LotusBrands < ActiveRecord::Base
  unloadable
  belongs_to :lotus_clients, :class_name => "LotusClients", :foreign_key => "lotus_clients_id"

  attr_accessible :name, :name_short, :description, :created_at, :updated_at

end