class LotusJobs < ActiveRecord::Base
  unloadable
  
  belongs_to :lotus_companies, :class_name => "LotusCompanies", :foreign_key => "lotus_company_id"

  belongs_to :lotus_brands, :class_name => "LotusBrands", :foreign_key => "lotus_brands_id"

  attr_accessible :name, :name_short, :description, :job_number, :job_frc, :created_at, :updated_at
end