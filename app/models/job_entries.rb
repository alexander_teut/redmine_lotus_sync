class JobEntries < ActiveRecord::Base
  unloadable
  attr_accessible :company, :client, :number, :description, :frc, :created_at, :updated_at
end
