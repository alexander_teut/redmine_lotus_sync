$(function(){
  var entry_type = {
    "Project" : [
       "Проектная документация",
       "Креатив и дизайн",
       "Верстка HTML/JavaScript",
       "Программирование FrontEnd",
       "Программирование BackEnd",
       "Тестирование",
       "Модерация",
       "Составление отчетов",
       "Копирайтинг",
       "Менеджмент проекта",
       "Консультации",
       "Встреча с клиентом",
       "Внутренняя встреча",
       "Разработка стратегии",
       "Разработка идеи",
       "Сборка презентации",
       "CRM политики и ТЗ",
       "Программирование CRM",
    ],
    "Other" : [
      "Внутренний проект",
      "Team management",
      "Серфинг интернета",
      "Простой в работе",
      "Персональное время",
      "Сокращённый день",
    ]
  };

  var entry_type_values = null;

  var current_entry_type = "";

  var is_same_button = true;

  var FILTERING_INTERVAL = 200;

  var is_selected_entry_type_set = false;
  var is_calendar_filter_set = false;
  var is_start_task_filter_set = false;
  var is_deadline_filter_set = false;

  setInterval(applyFilters, FILTERING_INTERVAL);
  $('#button_bar .lu_button:not(.fake_btn)').click(function () { 
    is_selected_entry_type_set = false;
    is_same_button = false;
  });

  function applyFilters(){
  	filterInLuxuryButtons();
  	filterCalendarItemsToChange();
    filterStartTask();
    filterDeadline();
  }

  function filterCalendarItemsToChange(){
  	if(is_calendar_filter_set)
  		return;
  	var time_entry_spent_on_lu = $('#time_entry_spent_on_lu');

  	if(!time_entry_spent_on_lu.length)
    {
      time_entry_spent_on_lu = $('#time_entry_spent_on');
      
      if(!time_entry_spent_on_lu.length)
  		  return;
    }

  	is_calendar_filter_set = true;

  	time_entry_spent_on_lu.datepicker("option","maxDate", new Date());
  }

  function filterStartTask(){
    if(is_start_task_filter_set)
      return;

    var issue_start_date_lu = $('#issue_start_date_lu');

    if(!issue_start_date_lu.length)
        return;

    is_start_task_filter_set = true;

    issue_start_date_lu.datepicker("option", "minDate", new Date());
    //issue_start_date_lu.datepicker("option", "maxDate", new Date());
  }

  function filterInLuxuryButtons() {
    if(is_selected_entry_type_set)
      return;

    var selected_entry_type = $("#issue_custom_field_values_21_lu");
    if(!selected_entry_type.length)
      return;

    is_selected_entry_type_set = true;

    var time_entry_activity_lu = $("select#time_entry_activity_id_lu");
    var time_entry_activity = time_entry_activity_lu;

    var time_entry_activity_simple = $("select#time_entry_activity_id");
    
    if(!time_entry_activity.length){
      if(time_entry_activity_simple.length)
        time_entry_activity = time_entry_activity_simple;
      else
        return;
    }

    if(!time_entry_activity.length)
      return;

    // Сохраняем id типов activity
    if(entry_type_values == null) {
      entry_type_values = {};
      for(var prop in entry_type)  {
        if (!entry_type.hasOwnProperty(prop))
            continue;
        var entry_type_property = entry_type[prop];
        entry_type_values[prop] = [];
        for (var i = 0; i < entry_type_property.length; ++i) {
          var opt_item = time_entry_activity.find("option").filter(function () { return $(this).html() == entry_type_property[i]; });
          if(opt_item.length) {
              entry_type_title = entry_type_property[i];
              entry_type_values[prop].push({ title : entry_type_title, value : opt_item.val() });
          }
        }
      }
    }

    // Делаем фильтрацию
    selected_entry_type.change(function() {
       update_entry_activities(time_entry_activity, $(this));
    });
    update_entry_activities(time_entry_activity, selected_entry_type);
  }

  function filterDeadline(){
    var issue_due_date_lu = $('#issue_due_date_lu');

    if(!issue_due_date_lu.length) {
      is_deadline_filter_set = false;
      return;
    } else if(is_deadline_filter_set) {
      return;
    }

    var deadline_label = $("th:contains('Deadline')").next('td');
    if(!deadline_label.length)
      return;
    
    is_deadline_filter_set = true;

    var deadline_label_html = deadline_label.html();
    if(deadline_label_html) {
      var deadline_label_date = $.datepicker.parseDate("dd.mm.yy", deadline_label_html);
      issue_due_date_lu.datepicker("option", "maxDate", deadline_label_date);
    }

    var start_label = $("td.start-date");
    if(!start_label.length)
      return;

    var start_label_html = start_label.html();
    if(start_label_html) {
      var start_label_date = $.datepicker.parseDate("dd.mm.yy", start_label_html);
      issue_due_date_lu.datepicker("option", "minDate", start_label_date);
    }
  }

  function update_entry_activities(selector_item, selected_entry_type){
    var new_entry_type = selected_entry_type.val();

    //Если тип не менялся, то и список доступных activities остаётся прежним
    if(is_same_button && current_entry_type == new_entry_type)
        return;
    
    is_same_button = true;
    current_entry_type = new_entry_type;
    
    // Очистить опции
    selector_item.find("option").remove();

    if(current_entry_type.length) {
      //Показать выбранные
      var values_to_select = entry_type_values[current_entry_type];
      load_values_to_select(values_to_select, selector_item);
    } else {
      //Показать все
      $('<option/>').text("--- Выберите ---").appendTo(selector_item);
      for(var prop in entry_type_values){
        if (!entry_type_values.hasOwnProperty(prop))
          continue;
        var values_to_select = entry_type_values[prop];
        load_values_to_select(values_to_select, selector_item);
      }
    }
  }

  function load_values_to_select(values_to_select, selector_item){
    for(var i = 0, len = values_to_select.length; i < len; i++){
      var value_to_select = values_to_select[i];
      $('<option/>', { value : value_to_select.value }).text(value_to_select.title).appendTo(selector_item);
    }
  }
});