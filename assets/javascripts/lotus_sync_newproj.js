$.extend({
 UrlVars : null,
 getUrlVars : function(){
 	if(this.UrlVars) return this.UrlVars;
 	this.UrlVars = {};
 	var index_separator = window.location.href.indexOf('?') + 1;
 	if (index_separator) {
		var hashes = window.location.href.slice(index_separator).split('&');
		var i = hashes.length;
		while(i--){
			var hash = decodeURIComponent(hashes[i]).split('=');
			this.UrlVars[hash[0]] = hash[1];
		}
	}
	return this.UrlVars;
 },
 getUrlVar : function(key) {
	if(!key) return null;
	var vars = this.getUrlVars();
	return (typeof(vars[key]) != "undefined") ? vars[key] : "";
 }
});

$(function(){
	function is_new_project_page() {
		var new_projects_url = "/projects/new";
		return window.location.href.indexOf(new_projects_url);
	}
	function find_edit_by_label(label) {
		return $("span:contains('"+label+"')").parent().parent().find('input');
	}
	function set_options_by_get_params(params){
		for(var params_index in params){
			find_edit_by_label(params[params_index]).val($.getUrlVar(params_index));
		}
	}
	if(is_new_project_page()) {
		var new_project_get_params = {
			"brand" : "Brand",
			"client" : "Client",
			"job_number" : "JobNumber"
		};
		set_options_by_get_params(new_project_get_params);
	}
});