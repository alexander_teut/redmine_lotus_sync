require_dependency 'lotus_sync/lotus_sync'

require 'redmine'

Redmine::Plugin.register :redmine_lotus_sync do
  name 'Redmine Lotus Sync plugin'
  author 'Alexander Teut'
  description 'Sync Redmine time spent with Lotus' 
  version '1.4.0'
  requires_redmine :version_or_higher => '2.2.1'
  url 'http://tp.letoagency.ru/'
  author_url 'http://github.com/rikkimongoose'

  settings :default => {
    :lotus_output_active => true,
    :lotus_output_email_active => true,
    :lotus_output_dir => 'log/leto_ts_',
    :lotus_email_to => 'ts@arkconnect.ru',
    :lotus_email_subject => 'Lotus XML ',
    :lotus_email_server => 'smtp.mastermail.ru',
    :lotus_email_port => '25',
    :lotus_email_from => 'administrator@yainthecity.ru',
    :lotus_email_user => 'administrator@yainthecity.ru',
    :lotus_email_pass => 'COHemin:6qua',
    :do_load_jobs_from_xml => true,
    :load_jobs_from_xml_path => 'lotus_info/Jobs.xml',
    }, :partial => 'settings/redmine_lotus_sync_config'

  permission :view_lotus_job, :lotus_jobs => :index, :require => :loggedin
  permission :reload_lotus_job, :lotus_jobs => :reload, :require => :loggedin

  menu :top_menu, :dayoff_entries, {:controller => 'dayoff_entries_setup', :action => 'index'}, :caption => :label_dayoff_menu
  menu :top_menu, :lotus_jobs, { :controller => 'lotus_jobs', :action => 'index'}, :caption => :label_lotus_jobs_menu, :if => Proc.new { User.current.allowed_to?(:view_lotus_job, nil, :global => true) }
end

ActionDispatch::Callbacks.to_prepare do
	require_dependency 'time_entry'

	# Guards against including the module multiple time (like in tests)
	# and registering multiple callbacks
	unless TimeEntry.included_modules.include? LotusSync::TimeEntryPatch
		TimeEntry.send(:include, LotusSync::TimeEntryPatch)
	end
end