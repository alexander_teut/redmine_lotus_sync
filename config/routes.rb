# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

get 'dayoff-entries', :to => 'dayoff_entries_setup#index'
post 'dayoff-entries', :to => 'dayoff_entries_setup#update'


post 'reload', :to => 'lotus_jobs#reload'
get 'reload', :to => 'lotus_jobs#reload'

post 'lotus-jobs-json', :to => 'lotus_jobs#lotus_jobs_list_json'
get 'lotus-jobs-json', :to => 'lotus_jobs#lotus_jobs_list_json'
get 'lotus-jobs', :to => 'lotus_jobs#index'