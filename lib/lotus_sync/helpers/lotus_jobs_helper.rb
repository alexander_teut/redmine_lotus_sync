module LotusSync
  class LotusJobsHelper
    def self.clients_to_let_filter
      ["ASF", "Inbrief", "Leto"]
    end

    def self.new_project_url
      "/projects/new"
    end

    def self.js_columns
      ['lotus_jobs.id', 'lotus_jobs.job_number', 'lotus_clients.name', 'lotus_brands.name', 'lotus_companies.name', 'lotus_jobs.description']
    end

    def self.prepare_order_config(order_config_params)
      if order_config_params.nil? then return nil end

      order_config_param_col_index = 1;

      cols_to_order_by = []

      order_config_params.each do |order_config_param|
        order_col = order_config_param[order_config_param_col_index]
        order_col_index = order_col[:column].to_i
        if self.js_columns.length <= order_col_index then next end
        cols_to_order_by << "%s %s" % [ self.js_columns[order_col_index], order_col[:dir] ]
      end

      {
        :column_to_sort => cols_to_order_by
      }
    end

    def self.lotus_job_url(job_entry)
      job_number = job_entry.job_number

      if not clients_to_let_filter.include? job_entry.lotus_companies.name then return job_number end

      params = {
        :job_number => job_number,
        :client => job_entry.lotus_brands.lotus_clients.name,
        :brand => job_entry.lotus_brands.name
      }

      "<a href='#{new_project_url}?#{params.to_query}' class='icon icon-add'></a>#{job_number}"
    end

    def self.prepare_raw_job_entries(search_config = nil, order_config = nil)
      lotus_jobs = order_config.nil? ? LotusJobs.joins(:lotus_brands, :lotus_companies, lotus_brands: :lotus_clients).order('lotus_jobs.id').reverse_order
                                     : LotusJobs.joins(:lotus_brands, :lotus_companies, lotus_brands: :lotus_clients).order(order_config[:column_to_sort])
      if !search_config.nil?
        search_text = search_config[:value]
        if !search_text.empty?
          lotus_jobs = lotus_jobs.where(
            "job_number LIKE :search_text
            OR lotus_jobs.description LIKE :search_text
            OR (`lotus_brands`.`id` IS NOT NULL AND `lotus_brands`.`name` LIKE :search_text)
            OR (`lotus_companies`.`id` IS NOT NULL AND `lotus_companies`.`name` LIKE :search_text)
            OR (`lotus_clients`.`id` IS NOT NULL AND `lotus_clients`.`name` LIKE :search_text)
            " , search_text: "%#{search_text}%")
        end
      end
      lotus_jobs
    end

    def self.lotus_jobs_list(draw, offset, limit, search_config, order_config)
      job_numbers = []

      lotus_jobs_all_count = LotusJobs.count

      job_entries = self.prepare_raw_job_entries(search_config, self.prepare_order_config(order_config))
      job_entries_count = job_entries.count
      job_entries_part = job_entries.offset(offset).limit(limit)

      job_entries_part.each do |job_entry|
        job_numbers << [job_entry.id, 
                        self.lotus_job_url(job_entry),
                        job_entry.lotus_brands.name,
                        job_entry.lotus_brands.lotus_clients.name,
                        job_entry.lotus_companies.name,
                        job_entry.description]
      end
      {
        :draw => draw,
        :recordsTotal => lotus_jobs_all_count,
        :recordsFiltered => job_entries_count,
        :data => job_numbers
      }
    end
  end
end