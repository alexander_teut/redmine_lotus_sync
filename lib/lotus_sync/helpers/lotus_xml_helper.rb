module LotusSync
  module LotusXmlHelper
      class LotusXmlHelper
        @@plugin_settings = { }
        @@xml_file_full_name = ""

        def initialize(settings)
          @@plugin_settings = settings
        end

        def has_output_settings
          @@plugin_settings[:lotus_output_active] or @@plugin_settings[:lotus_output_email_active]
        end

        def generate_xml_for_lotus
          if has_output_settings then
            lotus_xml = xml_for_lotus lotus_data
            save_xml_for_lotus lotus_xml if @@plugin_settings[:lotus_output_active]          
            email_for_lotus lotus_xml if @@plugin_settings[:lotus_output_email_active]
          end
        end

        def save_xml_for_lotus(xml_str)
          @@xml_file_full_name = Rails.root.join("%s%s.xml" % [@@plugin_settings[:lotus_output_dir], Time.now.strftime("%d.%m.%Y")]).to_s
          File.open(@@xml_file_full_name, "w") do |f|
            f.write(xml_str)
          end
          xml_str
        end

        def lotus_data
          @lotus_xml_items = []
          time_range = Time.now.midnight..Time.now

          get_issues_for_lotus(@lotus_xml_items, time_range)

          @lotus_xml_items.sort! {|x,y| y[:Date] <=> x[:Date] }

          get_dayoffs_for_lotus(@lotus_xml_items, time_range)

          @lotus_xml_items
        end

        def get_issues_for_lotus(lotus_xml_items, time_range)
          project_field_JobNumber = ProjectCustomField.where(:name => "JobNumber").first
          project_field_BriefNumber = ProjectCustomField.where(:name => "BriefNumber").first
          project_field_Client = ProjectCustomField.where(:name => "Client").first
          project_field_Brand = ProjectCustomField.where(:name => "Brand").first
          project_field_ContractWith = ProjectCustomField.where(:name => "ContractWith").first
          issue_field_FRC = IssueCustomField.where(:name => "FRC").first
          project_field_EntryType = IssueCustomField.where(:name => "EntryType").first
          user_lotus_name = UserCustomField.where(:name => "Name in Lotus").first
          lotus_activity_title = TimeEntryActivityCustomField.where(:name => "Lotus item").first
          
          TimeEntry.where(:updated_on => time_range).each do |time_entry|
            time_entry_user = User.where(:id => time_entry.user_id).first
            project_item = Project.where(:id => time_entry.project_id).first 
            issue_item = Issue.where(:id => time_entry.issue_id).first
            activity_item = TimeEntryActivity.where(:id => time_entry.activity_id).first

            if time_entry_user.nil? or issue_item.nil? or project_item.nil? or activity_item.nil? then next end
            
            project_field_JobNumber_value = CustomValue.where(:customized_type => "Project", :customized_id => project_item.id, :custom_field_id => project_field_JobNumber.id).first unless project_field_JobNumber.nil?
            project_field_BriefNumber_value = CustomValue.where(:customized_type => "Project", :customized_id => project_item.id, :custom_field_id => project_field_BriefNumber.id).first unless project_field_BriefNumber.nil?
            project_field_Client_value = CustomValue.where(:customized_type => "Project", :customized_id => project_item.id, :custom_field_id => project_field_Client.id).first unless project_field_Client.nil?
            project_field_Brand_value = CustomValue.where(:customized_type => "Project", :customized_id => project_item.id, :custom_field_id => project_field_Brand.id).first unless project_field_Brand.nil?
            project_field_ContractWith_value = CustomValue.where(:customized_type => "Project", :customized_id => project_item.id, :custom_field_id => project_field_ContractWith.id).first unless project_field_ContractWith.nil?
            issue_field_FRC_value = CustomValue.where(:customized_type => "Issue", :customized_id => issue_item.id, :custom_field_id => issue_field_FRC.id).first unless issue_field_FRC.nil?
            issue_field_EntryType_value = CustomValue.where(:customized_type => "Issue", :customized_id => issue_item.id, :custom_field_id => project_field_EntryType.id).first unless project_field_EntryType.nil?
            user_full_name_value = CustomValue.where(:customized_type => "Principal", :customized_id => time_entry_user.id, :custom_field_id => user_lotus_name.id).first unless user_lotus_name.nil?
            lotus_full_name_value = CustomValue.where(:customized_type => "Enumeration", :customized_id => activity_item.id, :custom_field_id => lotus_activity_title.id).first unless lotus_activity_title.nil?

            lotus_xml_items << {
              :Date => localise_date(time_entry.updated_on),
              :Hours => time_entry.hours,
              :EntryType => issue_field_EntryType_value.nil? ? '' : issue_field_EntryType_value.value,
              :BriefNumber =>  project_field_BriefNumber_value.nil? ? '' : project_field_BriefNumber_value.value,
              :JobNumber => project_field_JobNumber_value.nil? ? '' : project_field_JobNumber_value.value,
              :Client => project_field_Client_value.nil? ? '' : project_field_Client_value.value,
              :Brand => project_field_Brand_value.nil? ? '' : project_field_Brand_value.value,
              :DescriptionOfWork => (lotus_full_name_value.nil? or lotus_full_name_value.value.empty?) ? (activity_item.nil? ? '' : activity_item.name) : lotus_full_name_value.value,
              :Comments => time_entry.comments,
              :FRC => issue_field_FRC_value.nil? ? '' : issue_field_FRC_value.value,
              :Company => '',
              :Email =>  time_entry_user.mail,
              :Person => (user_full_name_value.nil? or user_full_name_value.value.empty?) ? ("%s %s" % [time_entry_user.firstname, time_entry_user.lastname]) : user_full_name_value.value,
              :RedmineTaskID => "#%s" % issue_item.id
            }
          end
        end

        def get_dayoffs_for_lotus(lotus_xml_items, time_range)
          DayoffEntries.where(:updated_at => time_range).order(:updated_at).each do |dayoff_entry|
            dayoffs_to_lotus(lotus_xml_items, dayoff_entry)
          end
        end

        def xml_for_lotus(source_data)
          builder = Nokogiri::XML::Builder.new(:encoding => "UTF-8") do |xml|
            xml.root {
              xml.items {
                source_data.each do |lotus_xml_item|
                  item_to_xml(xml, lotus_xml_item)
                end
              }
            }
          end
          builder.to_xml
        end

        def item_to_xml(xml, lotus_xml_item)
            xml.item {
                xml.Date_              lotus_xml_item[:Date]
                xml.Hours_             lotus_xml_item[:Hours]
                xml.EntryType_         lotus_xml_item[:EntryType]
                xml.BriefNumber_       lotus_xml_item[:BriefNumber]
                xml.JobNumber_         lotus_xml_item[:JobNumber]
                xml.Client_            lotus_xml_item[:Client]
                xml.Brand_             lotus_xml_item[:Brand]
                xml.DescriptionOfWork_ lotus_xml_item[:DescriptionOfWork]
                xml.Comments_          lotus_xml_item[:Comments]
                xml.FRC_               lotus_xml_item[:FRC]
                xml.Company_           lotus_xml_item[:Company]
                xml.Email_             lotus_xml_item[:Email]
                xml.Person_            lotus_xml_item[:Person]
                xml.RedmineTaskID_     lotus_xml_item[:RedmineTaskID]
            }
        end

        def localise_date(date)
          options = {}
          #options[:format] = Setting.date_format unless Setting.date_format.blank?
          options[:locale] = User.current.language unless User.current.language.blank?
          ::I18n.l(date.localtime, options)
        end

        def email_for_lotus(xml_str)
          Pony.mail({
              :to => @@plugin_settings[:lotus_email_to], #'ateut@arkgroup.ru',
              :from =>  @@plugin_settings[:lotus_email_from], #'administrator@yainthecity.ru',
              :subject => "%s%s" % [@@plugin_settings[:lotus_email_subject], Time.now.strftime("%d.%m.%Y")], #'XML loaded'
              :body => xml_str,
              :via => :smtp,
              #:attachments => {File.basename(@@xml_file_full_name) => File.read(@@xml_file_full_name)},
              :via_options => {
                :address        =>  @@plugin_settings[:lotus_email_server], #'smtp.mastermail.ru',
                :port           =>  @@plugin_settings[:lotus_email_port], #'25',
                :user_name      => @@plugin_settings[:lotus_email_user],  #'administrator@yainthecity.ru',
                :password       => @@plugin_settings[:lotus_email_pass],  #'COHemin:6qua',
                :authentication => :plain # :plain, :login, :cram_md5, no auth by default
              }
            })
          xml_str
        end

        def dayoffs_to_lotus(lotus_xml_items, dayoff_entry)
          dayoff_entry.from.to_date.upto(dayoff_entry.to.to_date) do |date|
            lotus_xml_items << dayoff_to_lotus(dayoff_entry, date)
          end
        end

        def dayoff_to_lotus(dayoff_entry, date_of_dayoff)
            dayoff_user = User.where(:id => dayoff_entry[:user_id]).first
            user_lotus_name = UserCustomField.where(:name => "Name in Lotus").first
            user_full_name_value = CustomValue.where(:customized_type => "Principal", :customized_id => dayoff_user.id, :custom_field_id => user_lotus_name.id).first unless user_lotus_name.nil?
            
            {
              :Date => localise_date(date_of_dayoff.to_time),
              :Hours => workhours,
              :EntryType => '',
              :BriefNumber => '',
              :JobNumber => '',
              :Client => '',
              :Brand => '',
              :DescriptionOfWork => dayoff_entry.activity_type,
              :Comments => '',
              :FRC => 'Leto',
              :Company => '',
              :Email =>  dayoff_user.mail,
              :Person => (user_full_name_value.nil? or user_full_name_value.value.empty?) ? ("%s %s" % [dayoff_user.firstname, dayoff_user.lastname]) : user_full_name_value.value,
              :RedmineTaskID => ''
            }
        end

        def workhours
          8
        end
      end
  end
end