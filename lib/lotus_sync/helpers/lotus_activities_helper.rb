module LotusSync
  class LotusActivitiesHelper
    
    include I18n

    def self.prepare_dayoff_entry_timerange(dayoff_entry)
      dayoff_entry.from = dayoff_entry.from.beginning_of_day
      dayoff_entry.to = dayoff_entry.to.end_of_day
      dayoff_entry
    end

    def self.actions_between(from, to)
      dayoff_items = DayoffEntries.where("user_id = :user_id AND `from` <= :to2 AND `to` >= :from2",
        {
          user_id: User.current.id,
          from2: from,
          to2: to
        }).order("`to`")

      if !dayoff_items.empty? then return self.dayoff_to_existing_item dayoff_items.first end

      time_range = from..to
      timeentry_items = TimeEntry.where(:updated_on => time_range, :user_id => User.current.id).order(:updated_on)

      if !timeentry_items.empty? then return self.timeentry_to_existing_item timeentry_items.first end

      return nil
    end

    def self.dayoff_to_existing_item(item)
      {
        :activity_name => item.activity_type,
        :from => item.from.strftime("%Y-%m-%d"),
        :to => item.to.strftime("%Y-%m-%d"),
        :type => "dayoff",
        :issue_name => ''
      }
    end

    def self.timeentry_to_existing_item(item)
      activity_item = TimeEntryActivity.where(:id => item.activity_id).first
      issue_item = Issue.where(:id => item.issue_id).first

      {
        :activity_name => activity_item.nil? ? '' : activity_item.name,
        :from => item.updated_at.strftime("%Y-%m-%d"),
        :to => item.updated_at.strftime("%Y-%m-%d"),
        :type => "timeentry",
        :issue_name => issue_item.nil? ? '' : issue_item.subject
      }
    end
  end
end