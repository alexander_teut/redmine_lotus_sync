module LotusSync
  module LotusDataHelper
    class LotusDataHelper
      @@plugin_settings = { }
      def plugin_settings; @@plugin_settings end

      def initialize(settings)
        @@plugin_settings = settings
      end

      def sync_db_data
        @xml_data = load_xml_from_file
        return if @xml_data.nil?

        @xml_data.xpath("//Job").each do |job|
          parse_xml_job_item job
        end
      end

      def load_xml_from_file
        @xml_doc = nil
        File.open(@@plugin_settings[:load_jobs_from_xml_path], "r") do |f|
          @xml_doc = Nokogiri::XML(f)
        end
        @xml_doc
      end

      def load_job_data(job_number)
      	job = LotusJobs.where(:job_number => job_number).first
      	if job.nil?
      		job = LotusJobs.new(:job_number => job_number, :created_at => Time.now, :updated_at => Time.now)
      		job.save
      	end
      	job
      end

      def load_company_data(company_name)
      	company = LotusCompanies.where(:name => company_name).first
      	if company.nil?
      		company = LotusCompanies.new(:name => company_name, :created_at => Time.now, :updated_at => Time.now)
      		company.save
      	end
      	company
      end

      def load_client_data(titles)
      	titles_arr = titles.split('|')
      	full_title = titles_arr[0]
      	short_title = titles.length > 1 ? titles_arr[1] : ''

      	data = LotusClients.where(:name => full_title).first

      	if data.nil?
      		data = LotusClients.new(:name => full_title, :name_short => short_title, :created_at => Time.now, :updated_at => Time.now)
      		data.save
      	end
      	data
      end

      def load_brand_data(titles)
      	titles_arr = titles.split('|')
      	full_title = titles_arr[0]
      	short_title = titles.length > 1 ? titles_arr[1] : ''

      	data = LotusBrands.where(:name => full_title).first

      	if data.nil?
      		data = LotusBrands.new(:name => full_title, :name_short => short_title, :created_at => Time.now, :updated_at => Time.now)
      		data.save
      	end
      	data
      end

      def parse_xml_job_item(job_item)
        is_new = false
        xml_param = lambda { |key| job_item.xpath(key).inner_html }
        job_data = load_job_data xml_param.call 'JobNumber'
        job_data_changed = false

        job_desc = xml_param.call 'JobDesc'

        if job_data.description != job_desc
        	job_data.description = job_desc
        	job_data_changed = true
        end

        company_data = load_company_data xml_param.call 'Company'

        if job_data.lotus_company_id != company_data.id
        	job_data.lotus_company_id = company_data.id
        	job_data_changed = true
        end

        client_data = load_client_data xml_param.call 'Client'

        brand_data = load_brand_data xml_param.call 'Brand'
        brand_data_changed = false

        if brand_data.lotus_clients_id != client_data.id
        	brand_data.lotus_clients_id = client_data.id
        	brand_data_changed = true
        end

        brand_data.save if brand_data_changed

        if job_data.lotus_brands_id != brand_data.id
        	job_data.lotus_brands_id = brand_data.id
        	job_data_changed = true
        end

        job_data.save if job_data_changed
      end
    end
  end
end