Rails.configuration.to_prepare do
  require_dependency 'lotus_sync/helpers/lotus_xml_sender'
end

module LotusSync
  module TimeEntryPatch
    include Redmine::I18n

    def self.included(base) # :nodoc:
      base.extend(ClassMethods)
      base.send(:include, InstanceMethods)
      
      # Same as typing in the class
      base.class_eval do
        unloadable # Send unloadable so it will not be unloaded in development
        
        after_save :update_lotus_from_issue
        # Add visible to Redmine 0.8.x
        unless respond_to?(:visible)
          named_scope :visible, lambda {|*args| { :include => :project,
          :conditions => Project.allowed_to_condition(args.first || User.current, :view_issues) } }
        end
      end
    end

    module ClassMethods
    end

    module InstanceMethods
      # This will update the KanbanIssues associated to the issue
      def update_lotus_from_issue
        lotus_methods = LotusXmlHelper::LotusXmlHelper.new(Setting.plugin_redmine_lotus_sync)
        lotus_methods.generate_xml_for_lotus
      end
    end 
  end
end