# Deprecated hook

module LotusSync
  module Hooks
	class LotusSyncHookJs < Redmine::Hook::ViewListener
		render_on(:view_layouts_base_html_head, :partial => "redmine_lotus_sync/html_head")
	end
  end
end