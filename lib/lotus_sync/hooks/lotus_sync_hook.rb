Rails.configuration.to_prepare do
  require_dependency 'lotus_sync/helpers/lotus_xml_sender'
end

module LotusSync
  module Hooks
	class LotusSyncHook < Redmine::Hook::ViewListener
		@settingsdayoffs_to_lotus

      	def controller_dayoff_entries_after_save(params)
      		@settings = Setting.plugin_redmine_lotus_sync || {}
        	send_xml_to_lotus params[:dayoff_entry]
		end

		def controller_lotus_jobs_update(params)
      		@settings = Setting.plugin_redmine_lotus_sync || {}
      		load_xml_jobs_data
		end

		def send_xml_to_lotus(dayoff_entry)
			if @settings[:lotus_output_active]
				_send_xml_to_lotus dayoff_entry
			end
		end

		def load_xml_jobs_data
			if @settings[:do_load_jobs_from_xml]
				_load_xml_jobs_data
			end
		end

		def _send_xml_to_lotus(dayoff_entry)
        	lotus_methods = LotusXmlHelper::LotusXmlHelper.new(@settings)
        	lotus_methods.generate_xml_for_lotus
		end

		def _load_xml_jobs_data
        	lotus_methods = LotusDataHelper::LotusDataHelper.new(@settings)
        	lotus_methods.sync_db_data
		end
	end
  end
end