Rails.configuration.to_prepare do
	require_dependency 'lotus_sync/time_entry_patch'
	require_dependency 'lotus_sync/helpers/lotus_xml_helper'
	require_dependency 'lotus_sync/helpers/lotus_data_helper'

	require_dependency 'lotus_sync/hooks/lotus_sync_hook'
	require_dependency 'lotus_sync/hooks/lotus_sync_hook_js'
end

module LotusSync
	class LotusSync
	end
end