class CreateLotusJobs < ActiveRecord::Migration
  def self.up
    create_table :lotus_jobs do |t|

      t.belongs_to :lotus_company, :class_name => "LotusCompanies", :foreign_key => "lotus_company_id"
      
      t.belongs_to :lotus_brands, :class_name => "LotusBrands", :foreign_key => "lotus_brands_id"

      t.string :name

      t.text :description

      t.string :job_number

      t.string :job_frc

      t.timestamps
      
    end

    add_index :lotus_jobs, :name
    
    add_index :lotus_jobs, :job_number

  end

  def self.down
    drop_table :lotus_jobs
  end
end
