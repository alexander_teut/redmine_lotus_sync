class CreateLotusCompanies < ActiveRecord::Migration
  def self.up
    create_table :lotus_companies do |t|

      t.string :name

      t.timestamps
      
    end

    add_index :lotus_companies, :name
    
  end

  def self.down
    drop_table :lotus_companies
  end
end
