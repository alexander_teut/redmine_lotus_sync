class CreateLotusBrands < ActiveRecord::Migration
  def self.up
    create_table :lotus_brands do |t|

      t.belongs_to :lotus_clients, :class_name => "LotusClients", :foreign_key => "lotus_clients_id"

      t.string :name

      t.string :name_short

      t.string :description

      t.timestamps
      
    end

    add_index :lotus_brands, :name

    add_index :lotus_brands, :name_short
    
  end

  def self.down
    drop_table :lotus_brands
  end
end
