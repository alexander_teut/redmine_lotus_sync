class CreateLotusClients < ActiveRecord::Migration
  def self.up
    create_table :lotus_clients do |t|

      t.string :name

      t.string :name_short

      t.string :description

      t.timestamps
      
    end

    add_index :lotus_clients, :name

    add_index :lotus_clients, :name_short
  end

  def self.down
    drop_table :lotus_clients
  end
end
