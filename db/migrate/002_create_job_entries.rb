class CreateJobEntries < ActiveRecord::Migration
  def self.up
    create_table :job_entries do |t|

      t.string :company

      t.string :client

      t.string :number

      t.string :description

      t.string :frc

      t.timestamps
      
    end

    add_index :job_entries, :company

    add_index :job_entries, :client

    add_index :job_entries, :number

    add_index :job_entries, :frc
  end

  def self.down
    drop_table :job_entries
  end
end
