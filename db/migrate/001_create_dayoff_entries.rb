class CreateDayoffEntries < ActiveRecord::Migration
  def self.up
    create_table :dayoff_entries do |t|

      t.integer :user_id

      t.string :activity_type

      t.datetime :to

      t.datetime :from

      t.timestamps
    end

    add_index :dayoff_entries, :user_id

    add_index :dayoff_entries, :activity_type
    
  end

  def self.down
    drop_table :dayoff_entries
  end
end
