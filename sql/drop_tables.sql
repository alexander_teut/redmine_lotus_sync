DROP TABLE lotus_jobs;
DROP TABLE lotus_brands;
DROP TABLE lotus_clients;
DROP TABLE lotus_companies;
DROP TABLE dayoff_entries;
DROP TABLE job_entries;

DELETE FROM redmine.schema_migrations WHERE version LIKE '%redmine_lotus_sync%';